﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Comments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ComentsModel_AspNetUsers_UploaderId",
                table: "ComentsModel");

            migrationBuilder.DropForeignKey(
                name: "FK_ComentsModel_Videos_VideoModelId",
                table: "ComentsModel");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ComentsModel",
                table: "ComentsModel");

            migrationBuilder.RenameTable(
                name: "ComentsModel",
                newName: "Comments");

            migrationBuilder.RenameIndex(
                name: "IX_ComentsModel_VideoModelId",
                table: "Comments",
                newName: "IX_Comments_VideoModelId");

            migrationBuilder.RenameIndex(
                name: "IX_ComentsModel_UploaderId",
                table: "Comments",
                newName: "IX_Comments_UploaderId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Comments",
                table: "Comments",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_AspNetUsers_UploaderId",
                table: "Comments",
                column: "UploaderId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Videos_VideoModelId",
                table: "Comments",
                column: "VideoModelId",
                principalTable: "Videos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_AspNetUsers_UploaderId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Videos_VideoModelId",
                table: "Comments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Comments",
                table: "Comments");

            migrationBuilder.RenameTable(
                name: "Comments",
                newName: "ComentsModel");

            migrationBuilder.RenameIndex(
                name: "IX_Comments_VideoModelId",
                table: "ComentsModel",
                newName: "IX_ComentsModel_VideoModelId");

            migrationBuilder.RenameIndex(
                name: "IX_Comments_UploaderId",
                table: "ComentsModel",
                newName: "IX_ComentsModel_UploaderId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ComentsModel",
                table: "ComentsModel",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ComentsModel_AspNetUsers_UploaderId",
                table: "ComentsModel",
                column: "UploaderId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ComentsModel_Videos_VideoModelId",
                table: "ComentsModel",
                column: "VideoModelId",
                principalTable: "Videos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
