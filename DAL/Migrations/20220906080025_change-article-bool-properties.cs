﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class changearticleboolproperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Public",
                table: "Articles",
                newName: "IsPublic");

            migrationBuilder.RenameColumn(
                name: "Commentable",
                table: "Articles",
                newName: "IsCommentable");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsPublic",
                table: "Articles",
                newName: "Public");

            migrationBuilder.RenameColumn(
                name: "IsCommentable",
                table: "Articles",
                newName: "Commentable");
        }
    }
}
