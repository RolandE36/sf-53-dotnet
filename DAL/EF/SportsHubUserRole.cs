﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.EF
{
    public class SportsHubUserRole: IdentityRole 
    {
        public SportsHubUserRole(string name): base(name) { }
    }
}
