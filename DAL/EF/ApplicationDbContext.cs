using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace DAL.EF
{
    public class ApplicationDbContext : IdentityDbContext<SportsHubUser, SportsHubUserRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<TestModel> TestModels { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Subcategory> Subcategories { get; set; }

        public DbSet<Team> Teams { get; set; }

        
        //Gergana Articles
        public DbSet<Article> Articles { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<ImageUrl> Images { get; set; }

        public DbSet<VideoModel> Videos { get; set; }
        public DbSet<ComentsModel> Comments { get; set; }
    }
}
