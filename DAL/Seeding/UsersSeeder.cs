﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using DAL.EF;

namespace DAL.Seeding
{
    public class UsersSeeder : ISeeder
    {
        public async Task SeedAsync(ApplicationDbContext dbContext, IServiceProvider serviceProvider)
        {
            var userManager = (UserManager<SportsHubUser>)serviceProvider.GetService(typeof(UserManager<SportsHubUser>));

            // Seeding admin
            string adminEmail = "admin@email.com";
            if (await userManager.FindByEmailAsync(adminEmail) == null)
            {
                SportsHubUser user = new SportsHubUser
                {
                    UserName = adminEmail,
                    Email = adminEmail,
                    FirstName = "Admin",
                    LastName = "Admin",
                    ProfilePicturePath = "defaultProfilePicture.jpg"
                };

                IdentityResult identityResult = await userManager.CreateAsync(user, "a123456A!");

                if (identityResult.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, "Admin");
                }
            }

            // Seeding user
            string userEmail = "user@email.com";
            if (await userManager.FindByEmailAsync(userEmail) == null)
            {
                SportsHubUser user = new SportsHubUser
                {
                    UserName = userEmail,
                    Email = userEmail,
                    FirstName = "User",
                    LastName = "User",
                    ProfilePicturePath = "defaultProfilePicture.jpg"
                };

                IdentityResult identityResult = await userManager.CreateAsync(user, "b123456A!");

                if (identityResult.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, "User");
                }
            }
        }
    }
}
