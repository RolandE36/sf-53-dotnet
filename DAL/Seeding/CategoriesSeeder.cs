﻿namespace DAL.Seeding
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DAL.EF;
    using DAL.Models;

    public class CategoriesSeeder : ISeeder
    {
        public async Task SeedAsync(ApplicationDbContext dbContext, IServiceProvider serviceProvider)
        {
            if (dbContext.Categories.Any())
            {
                return;
            }

            var nbaTeam = new Team { Name = "NBA Team" };
            var nflTeam = new Team { Name = "NFL Team" };
            var mlbTeam = new Team { Name = "MLB Team", IsHidden = true };

            var nba1 = new Subcategory { Name = "NBA Sub1", Teams = new HashSet<Team> { nbaTeam } };
            var nba2 = new Subcategory { Name = "NBA Sub2" };
            var nfl1 = new Subcategory { Name = "NFL Sub1", Teams = new HashSet<Team> { nflTeam } };
            var nfl2 = new Subcategory { Name = "NFL Sub2", IsHidden = true };
            var mlb1 = new Subcategory { Name = "MLB Sub1", Teams = new HashSet<Team> { mlbTeam } };
            var mlb2 = new Subcategory { Name = "MLB Sub2" };

            await dbContext.Subcategories.AddAsync(nba1);
            await dbContext.Subcategories.AddAsync(nba2);
            await dbContext.Subcategories.AddAsync(nfl1);
            await dbContext.Subcategories.AddAsync(nfl2);
            await dbContext.Subcategories.AddAsync(mlb1);
            await dbContext.Subcategories.AddAsync(mlb2);

            await dbContext.Categories.AddAsync(new Category { Name = "NBA", IsHidden = true, Subcategories = new HashSet<Subcategory> { nba1, nba2 } });
            await dbContext.Categories.AddAsync(new Category { Name = "NFL", Subcategories = new HashSet<Subcategory> { nfl1, nfl2 } });
            await dbContext.Categories.AddAsync(new Category { Name = "MLB", Subcategories = new HashSet<Subcategory> { mlb1, mlb2 } });

            await dbContext.SaveChangesAsync();
        }
    }
}
