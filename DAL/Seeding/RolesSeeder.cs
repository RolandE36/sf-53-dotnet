﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using DAL.EF;

namespace DAL.Seeding
{
    public class RolesSeeder : ISeeder
    {
        public async Task SeedAsync(ApplicationDbContext dbContext, IServiceProvider serviceProvider)
        {
            var roleManager = (RoleManager<SportsHubUserRole>) serviceProvider.GetService(typeof(RoleManager<SportsHubUserRole>));

            // Creating admin role
            if (!await roleManager.RoleExistsAsync("Admin"))
            {
                await roleManager.CreateAsync(new SportsHubUserRole("Admin"));
            }

            // Creating user role
            if (!await roleManager.RoleExistsAsync("User"))
            {
                await roleManager.CreateAsync(new SportsHubUserRole("User"));
            }
        }
    }
}
