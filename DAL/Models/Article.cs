﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Models
{
  public  class Article
    {
        
        [Required]
        public int Id { get; set; }

        [Required]
        public string Alt { get; set; }

        [Required]
        public string Headline { get; set; }
        [Required]
        public string Caption { get; set; }

        [Required]
        public string Content { get; set; }

        public byte IsPublic { get; set; }


        public bool IsPublished { get; set; }

        public bool IsCommentable { get; set; }

        [Required]
        public string PictureUrl { get; set; } 

        public ImageUrl ImageUrl { get; set; } 

        [ForeignKey("SubcategoryId")]
        public int SubcategoryId { get; set; }

        public Subcategory SubCategory { get; set; }

        public int LocationId { get; set; }
        public Location Location { get; set; }

        [ForeignKey("TeamId")]
        public int TeamId { get; set; }
        public Team Team { get; set; }

    }
}
