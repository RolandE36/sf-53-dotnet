﻿using DAL.EF;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class VideoModel
    {
        [Key]
        public int Id { get; set; }
        public SportsHubUser Uploader { get; set; }
        [Required]
        public bool isPublished { get; set; } = false;
        [Required]
        public string Title { get; set; } = "Default Title";
        [Required]
        public bool Comments { get; set; } = false;
        [Required]
        public bool IsLink { get; set; }
        [Required]
        public string URL { get; set; }
        [Required]
        public DateTime PublishDate { get; set; }
        public int Views { get; set; }= 0;
        public virtual List<ComentsModel> Coments { get; set; } = new List<ComentsModel>();
    }
}
