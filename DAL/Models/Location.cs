﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

//Gergana Articles

namespace DAL.Models
{
  public  class Location
    {
        public Location()
        {
            this.Articles = new HashSet<Article>();
        }

        [Required]
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Article> Articles { get; set; }

    }
}
