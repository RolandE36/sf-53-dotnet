﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class Category
    {
        public Category()
        {
            this.Subcategories = new HashSet<Subcategory>();
            
            this.Articles = new HashSet<Article>();       
        
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsHidden { get; set; }

        public virtual ICollection<Subcategory> Subcategories { get; set; }

        public ICollection<Article> Articles{ get; set; }
    }
}
