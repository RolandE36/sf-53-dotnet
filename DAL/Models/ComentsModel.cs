﻿using DAL.EF;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Models
{
    public class ComentsModel
    {
        [Key]
        public int Id { get; set; }
        public SportsHubUser Uploader { get; set; }
        [Required]
        public string Details { get; set; }
        [Required]
        public DateTime PublishDate { get; set; }
    }
}
