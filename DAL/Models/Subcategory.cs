﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class Subcategory
    {
        public Subcategory()
        {
            this.Teams = new HashSet<Team>();
            this.Articles = new HashSet<Article>();
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsHidden { get; set; }

        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public virtual ICollection<Team> Teams { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
    }
}
