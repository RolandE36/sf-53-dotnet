﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class SearchItemModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public bool IsPictureUrl { get; set; }
        public string PictureUrl { get; set; }
        public List<string> CategorySequence { get; set; }
    }
}