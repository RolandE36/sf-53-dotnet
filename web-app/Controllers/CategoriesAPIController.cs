﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using BAL.Interfaces.Managers;
using System.Text.Json;
using System.Threading.Tasks;

namespace web_app.Controllers
{
    [Route("/api/categories")]
    [ApiController]
    public class CategoriesAPIController : ControllerBase
    {
        private readonly ICategoryManager _categoryManager;

        public CategoriesAPIController(ICategoryManager categoryManager)
        {
            this._categoryManager = categoryManager;
        }

        [Route("/api/categories/addCategories")]
        [HttpPost]
        public async Task AddCategories(object[] categories)
        {
            var newCategories = new List<string>();

            foreach (var category in categories)
            {
                newCategories.Add(category.ToString());
            }

            await this._categoryManager.AddCategories(newCategories);
        }

        [Route("/api/categories/addSubcategories")]
        [HttpPost]
        public async Task AddSubcategories(object[][] subcategories)
        {
            var newSubcategories = new Dictionary<string, List<string>>();

            foreach (var subcategory in subcategories)
            {
                var categoryName = subcategory[0].ToString();
                var subcategoryName = subcategory[1].ToString();

                if (!newSubcategories.ContainsKey(categoryName))
                {
                    newSubcategories[categoryName] = new List<string>();
                }

                newSubcategories[categoryName].Add(subcategoryName);
            }

            await this._categoryManager.AddSubcategories(newSubcategories);
        }

        [Route("/api/categories/addTeams")]
        [HttpPost]
        public async Task AddTeams(object[][] teams)
        {
            var newTeams = new Dictionary<string, List<string>>();

            foreach (var team in teams)
            {
                var subcategoryName = team[0].ToString();
                var teamName = team[1].ToString();

                if (!newTeams.ContainsKey(subcategoryName))
                {
                    newTeams[subcategoryName] = new List<string>();
                }

                newTeams[subcategoryName].Add(teamName);
            }

            await this._categoryManager.AddTeams(newTeams);
        }

        [Route("/api/categories/hideTeams")]
        [HttpPost]
        public async Task HideTeams(object[][] teams)
        {
            var hiddenCheckTeams = new List<KeyValuePair<string, bool>>();

            foreach (var team in teams)
            {
                var lastSavedName = team[0].ToString();
                var newName = Convert.ToBoolean(team[1].ToString());

                hiddenCheckTeams.Add(new KeyValuePair<string, bool>(lastSavedName, newName));
            }

            await this._categoryManager.HideTeams(hiddenCheckTeams);
        }

        [Route("/api/categories/hideSubcategories")]
        [HttpPost]
        public async Task HideSubcategories(object[][] subcategories)
        {
            var hiddenCheckSubcategories = new List<KeyValuePair<string, bool>>();

            foreach (var subcategory in subcategories)
            {
                var lastSavedName = subcategory[0].ToString();
                var newName = Convert.ToBoolean(subcategory[1].ToString());

                hiddenCheckSubcategories.Add(new KeyValuePair<string, bool>(lastSavedName, newName));
            }

            await this._categoryManager.HideSubcategories(hiddenCheckSubcategories);
        }

        [Route("/api/categories/hideCategories")]
        [HttpPost]
        public async Task HideCategories(object[][] categories)
        {
            var hiddenCheckCategories = new List<KeyValuePair<string, bool>>();

            foreach (var category in categories)
            {
                var lastSavedName = category[0].ToString();
                var newName = Convert.ToBoolean(category[1].ToString());

                hiddenCheckCategories.Add(new KeyValuePair<string, bool>(lastSavedName, newName));
            }

            await this._categoryManager.HideCategories(hiddenCheckCategories);
        }

        [Route("/api/categories/editCategories")]
        [HttpPost]
        public async Task EditCategories(object[][] categories)
        {
            var editedCategories = new List<KeyValuePair<string, string>>();

            foreach (var category in categories)
            {
                var lastSavedName = category[0].ToString();
                var newName = category[1].ToString();

                editedCategories.Add(new KeyValuePair<string, string>(lastSavedName, newName));
            }

            await this._categoryManager.EditCategories(editedCategories);
        }

        [Route("/api/categories/editSubcategories")]
        [HttpPost]
        public async Task EditSubcategories(object[][] subcategories)
        {
            var editedSubcategories = new List<KeyValuePair<string, string>>();

            foreach (var subcategory in subcategories)
            {
                var lastSavedName = subcategory[0].ToString();
                var newName = subcategory[1].ToString();

                editedSubcategories.Add(new KeyValuePair<string, string>(lastSavedName, newName));
            }

            await this._categoryManager.EditSubcategories(editedSubcategories);
        }

        [Route("/api/categories/editTeams")]
        [HttpPost]
        public async Task EditTeams(object[][] teams)
        {
            var editedTeams = new List<KeyValuePair<string, string>>();

            foreach (var team in teams)
            {
                var lastSavedName = team[0].ToString();
                var newName = team[1].ToString();

                editedTeams.Add(new KeyValuePair<string, string>(lastSavedName, newName));
            }

            await this._categoryManager.EditTeams(editedTeams);
        }

        [Route("/api/categories/moveTeams")]
        [HttpPost]
        public async Task MoveTeams(object[] bigObj)
        {
            var movedTeams = new List<KeyValuePair<string, string>>();
            var newTeams = new Dictionary<string, List<string>>();

            var teams = JsonSerializer.Deserialize<object[][]>(bigObj[0].ToString());
            var newTeamsObj = JsonSerializer.Deserialize<object[][]>(bigObj[1].ToString());

            foreach (var team in newTeamsObj)
            {
                var subcategoryName = team[0].ToString();
                var teamName = team[1].ToString();

                if (!newTeams.ContainsKey(subcategoryName))
                {
                    newTeams[subcategoryName] = new List<string>();
                }

                newTeams[subcategoryName].Add(teamName);
            }

            foreach (var team in teams)
            {
                var subcategoryName = team[0].ToString();
                var teamName = team[1].ToString();

                movedTeams.Add(new KeyValuePair<string, string>(subcategoryName, teamName));
            }

            await this._categoryManager.MoveTeams(movedTeams, newTeams);
        }

        [Route("/api/categories/moveSubcategories")]
        [HttpPost]
        public async Task MoveSubcategories(object[] bigObj)
        {
            var movedSubcategories = new List<KeyValuePair<string, string>>();
            var newSubcategories = new Dictionary<string, List<string>>();

            var subcategories = JsonSerializer.Deserialize<object[][]>(bigObj[0].ToString());
            var newSubcategoriesObj = JsonSerializer.Deserialize<object[][]>(bigObj[1].ToString());

            foreach (var subcategory in newSubcategoriesObj)
            {
                var categoryName = subcategory[0].ToString();
                var subcategoryName = subcategory[1].ToString();

                if (!newSubcategories.ContainsKey(categoryName))
                {
                    newSubcategories[categoryName] = new List<string>();
                }

                newSubcategories[categoryName].Add(subcategoryName);
            }

            foreach (var subcategory in subcategories)
            {
                var categoryName = subcategory[0].ToString();
                var subcategoryName = subcategory[1].ToString();

                movedSubcategories.Add(new KeyValuePair<string, string>(categoryName, subcategoryName));
            }

            await this._categoryManager.MoveSubcategories(movedSubcategories, newSubcategories);
        }

        [Route("/api/categories/deleteTeams")]
        [HttpPost]
        public async Task DeleteTeams(object[] teams)
        {
            var deletedTeams = new List<string>();

            foreach (var team in teams)
            {
                deletedTeams.Add(team.ToString());
            }

            await this._categoryManager.DeleteTeams(deletedTeams);
        }

        [Route("/api/categories/deleteSubcategories")]
        [HttpPost]
        public async Task DeleteSubcategories(object[] subcategories)
        {
            var deletedSubcategories = new List<string>();

            foreach (var subcategory in subcategories)
            {
                deletedSubcategories.Add(subcategory.ToString());
            }

            await this._categoryManager.DeleteSubcategories(deletedSubcategories);
        }

        [Route("/api/categories/deleteCategories")]
        [HttpPost]
        public async Task DeleteCategories(object[] categories)
        {
            var deletedCategories = new List<string>();

            foreach (var category in categories)
            {
                deletedCategories.Add(category.ToString());
            }

            await this._categoryManager.DeleteCategories(deletedCategories);
        }
    }
}
