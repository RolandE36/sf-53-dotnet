﻿using BAL.DTOs.Categories;
using BAL.Interfaces.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Linq;
using System.Threading.Tasks;
using static web_app.Common.GlobalConstants;

namespace web_app.Controllers
{
    //[Authorize(Roles = ADMINISTRATOR_ROLE_NAME)]
    public class CategoriesController : Controller
    {
        private readonly ICategoryManager _categoryManager;

        public CategoriesController(ICategoryManager categoryService)
        {
            this._categoryManager = categoryService;
        }

        public IActionResult Index()
        {
            var viewModel = new CategoriesIndexViewModel
            {
                Categories = this._categoryManager.GetAllCategories<CategoryViewModel>().ToList(),
            };

            foreach (var category in viewModel.Categories)
            {
                category.Subcategories = this._categoryManager.GetAllSubcategories<SubcategoryViewModel>(category.Id);

                foreach (var subcategory in category.Subcategories)
                {
                    subcategory.Teams = this._categoryManager.GetAllTeams<TeamViewModel>(subcategory.Id);
                }
            }
            
            return this.View(viewModel);
        }

        public ActionResult CategoriesListedPartial()
        {
            var viewModel = new CategoriesIndexViewModel
            {
                Categories = this._categoryManager.GetAllCategories<CategoryViewModel>().ToList(),
            };

            return PartialView("_CategoriesListedPartial", viewModel);
        }
    }
}
