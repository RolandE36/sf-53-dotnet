using BAL.DTOs;
using BAL.Interfaces.Managers;
using BAL.Managers;
using BAL.Mapping;
using DAL.EF;
using DAL.Repositories;
using DAL.Seeding;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace web_app
{
    public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
        }

		public IConfiguration Configuration { get; }

		
		public void ConfigureServices(IServiceCollection services)
		{
           
            services.Configure<RazorViewEngineOptions>(o =>
            {
                o.ViewLocationFormats.Add("/Pages/{1}/{0}" + RazorViewEngine.ViewExtension);
            });

            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddMvc()
                    .AddViewLocalization(
                        LanguageViewLocationExpanderFormat.Suffix,
                        options => { options.ResourcesPath = "Resources"; })
                    .AddDataAnnotationsLocalization();

            services.Configure<RequestLocalizationOptions>(
                options =>
                {
                    var supportedCultures = new List<CultureInfo>
                    {
                        new CultureInfo("en"),
                        new CultureInfo("bg")
                    };

                    options.DefaultRequestCulture = new RequestCulture("en");
                    options.SupportedCultures = supportedCultures;
                    options.SupportedUICultures = supportedCultures;
                }
            );

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            var connectionString = this.Configuration.GetConnectionString("DefaultConnection");
			services.AddDbContext<ApplicationDbContext>(options =>
			{
				options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
			});

            services.AddIdentity<SportsHubUser, SportsHubUserRole>(
				options => {
					options.SignIn.RequireConfirmedAccount = false;
				}
			)
			.AddRoles<SportsHubUserRole>()
			.AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddSignalR();

            services.AddRazorPages();

            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));

            services.AddTransient<ICategoryManager, CategoryManager>();
            services.AddTransient<ISearchManager, SearchManager>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UserManager<SportsHubUser> userManager, RoleManager<SportsHubUserRole> roleManager)
		{
            // Seed data on application startup
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                dbContext.Database.Migrate();
                new ApplicationDbContextSeeder().SeedAsync(dbContext, serviceScope.ServiceProvider).GetAwaiter().GetResult();
            }

            AutoMapperConfig.RegisterMappings(typeof(ErrorViewModel).GetTypeInfo().Assembly);

            app.UseDeveloperExceptionPage();

			app.UseHttpsRedirection();
			app.UseStaticFiles();
			app.UseRouting();

			app.UseAuthentication();
			app.UseAuthorization();

            app.Use(async (context, next) =>
            {
                var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
                var supportedLanguages = options.Value.SupportedCultures.Select(c => c.Name);
                string userLanguages = context.Request.Headers["Accept-Language"].ToString();
                string firstUserLanguage = userLanguages.Split(',').FirstOrDefault();
                if (!string.IsNullOrEmpty(firstUserLanguage) && supportedLanguages.Contains(firstUserLanguage))
                {
                    options.Value.SetDefaultCulture(firstUserLanguage);
                }
                await next();
            });

            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);

            app.UseEndpoints(endpoints =>
			{
                endpoints.MapRazorPages();
                endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "Pages/{controller=Home}/{action=Index}/{id?}");
            });
        }
	}
}
