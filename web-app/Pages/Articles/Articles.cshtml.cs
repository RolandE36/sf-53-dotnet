﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL.EF;
using DAL.Models;

namespace web_app.Pages.Shared
{
    public class ArticlesModel : PageModel
    {
        private readonly DAL.EF.ApplicationDbContext _context;
       

        public ArticlesModel(DAL.EF.ApplicationDbContext context)
        {
            _context = context;

        }

       
        [BindProperty]
        public Article CurrentArticle { get; set; }

        public IList<Article> Article { get;set; }

        public IList<Subcategory> Category { get; set; }
        public IList<Team> Team { get; set; }
        public IList<Location> Location { get; set; }

        public IList<ImageUrl> ImageUrl { get; set; }

        
        public async Task OnGetAsync()
        {
            Article = await _context.Articles.ToListAsync();

            Category = await _context.Subcategories.ToListAsync();
            Location = await _context.Locations.ToListAsync();
            Team = await _context.Teams.ToListAsync();
            ImageUrl = await _context.Images.ToListAsync();
         }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            var article = _context.Articles.Find(id);
            // Incorrect id
            if (id == null || article == null) return RedirectToPage("./Articles");

            // Delete Article
            _context.Articles.Remove(article);
            await _context.SaveChangesAsync();

            // Redirect to List of Articles
            return RedirectToPage("./Articles");
        }

    }
}
