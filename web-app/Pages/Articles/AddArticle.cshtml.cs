﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DAL.EF;
using DAL.Models;

namespace web_app.Pages
{
    public class AddArticleModel : PageModel
    {
        private readonly DAL.EF.ApplicationDbContext _context;

        public AddArticleModel(DAL.EF.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Subcategory> Category { get; set; } 
        public IList<Team> Team { get; set; }
        public IList<Location> Location { get; set; }

        public IActionResult OnGet()
        {
            
            Category = _context.Subcategories.ToList();
            Team = _context.Teams.ToList();
            Location = _context.Locations.ToList();
            return Page();
        }
        
        [BindProperty]
        public Article Article { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
           
            if (!ModelState.IsValid)
            {
                Category = _context.Subcategories.ToList();
                Team = _context.Teams.ToList();
                Location = _context.Locations.ToList();

                return Page(); //It is not validated yet
            }

            _context.Articles.Add(Article);  //add article 
            await _context.SaveChangesAsync();

            return RedirectToPage("./Articles"); 
        }
    }
}
