﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.EF;
using DAL.Models;

namespace web_app.Pages
{
    public class UpdateArtileModel : PageModel
    {
        private readonly DAL.EF.ApplicationDbContext _context;

       
        public IList<Subcategory> Category { get; set; }
        public IList<Team> Team { get; set; }
        public IList<Location> Location { get; set; }

        
        public UpdateArtileModel(DAL.EF.ApplicationDbContext context)
        {
            _context = context;

            Category = _context.Subcategories.ToList();
            Team = _context.Teams.ToList();
            Location = _context.Locations.ToList();
        }

      
        [BindProperty]
        public Article Article { get; set; }
        
        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            
            Article = await _context.Articles.FirstOrDefaultAsync(m => m.Id == id);
           

            if (Article == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Article).State = EntityState.Modified;

            try
            {
                
                await _context.SaveChangesAsync();
            }

            
            catch (DbUpdateConcurrencyException)
            {
              
                if (!ArticleExists(Article.Id))
                {
                    return NotFound();
                }
                
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Articles");
        }

       
        private bool ArticleExists(int id)
        {
            return _context.Articles.Any(e => e.Id == id);
        }
    }
}
