﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL.EF;
using DAL.Models;

namespace web_app.Pages.Articles
{
    public class ArticleViewModel : PageModel
    {
        private readonly DAL.EF.ApplicationDbContext _context;



        public ArticleViewModel(DAL.EF.ApplicationDbContext context)
        {
            _context = context;
        }

        public Article Article { get; set; }

        public IList<Article> Articles { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {

            Articles = await _context.Articles.ToListAsync();
            
            if (id == null)
            {
                return NotFound();
            }

            Article = await _context.Articles
                .Include(a => a.Location)
                .Include(a => a.SubCategory)
                .Include(a => a.Team).FirstOrDefaultAsync(m => m.Id == id);

            if (Article == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
