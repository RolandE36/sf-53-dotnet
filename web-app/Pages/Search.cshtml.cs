﻿using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using DAL.EF;
using DAL.Models;
using BAL.Interfaces.Managers;

namespace web_app.Pages
{
    public class Search : PageModel
    {
        private readonly ISearchManager _searchManager;

        public Search(ApplicationDbContext context, ISearchManager searchManager)
        {
            _searchManager = searchManager;
        }
        
        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }
        public IList<SearchItemModel> SearchItems { get; set; }
        
        private void SetViewData()
        {
            ViewData["SearchString"] = SearchString;
        }

        public void OnGet()
        {
            SetViewData();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (string.IsNullOrEmpty(SearchString))
            {
                return Page();
            }
            
            _searchManager.SearchString = SearchString;
            await _searchManager.FilterData();
            SearchItems = _searchManager.GetSearchResults();
            
            SetViewData();
            return Page();
        }
    }
}