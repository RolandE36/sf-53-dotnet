using BAL.DTOs.Categories;
using BAL.Interfaces.Managers;
using DAL.EF;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace web_app.Pages.Video
{
    public class VideoPageModel : PageModel
    {
        private readonly UserManager<SportsHubUser> _userManager;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly ApplicationDbContext _context;
        private readonly ICategoryManager _categoryManager;


        public VideoPageModel(ICategoryManager categoryService,
            UserManager<SportsHubUser> userManager,
            IWebHostEnvironment webHostEnvironment,
            ApplicationDbContext context)
        {
            _categoryManager = categoryService;
            _userManager = userManager;
            _webHostEnvironment = webHostEnvironment;
            _context = context;
        }
        public class InputModel
        {
            public string VideoURL { get; set; }

            public IFormFile VideoFile { get; set; }

        }
        public class Filter
        {
            public string Name { get; set; }
            public PublishedSelector Published { get; set; } = PublishedSelector.All;

        }

        public enum PublishedSelector
        {
            All,
            Unpublished,
            Published
        };


        public IEnumerable<VideoModel> Videos { get; set; }

        private bool isLink = false;
        private string Title = "Default Title";
        public bool isPreview;
        public CategoriesIndexViewModel Categories;

        [BindProperty]
        public InputModel Input { get; set; }

        
        [BindProperty]
        public Filter Filters { get; set; } = new Filter();


        public async Task<IActionResult> OnGetAsync(string searchString,PublishedSelector publishedFilter,bool isPreview=false)
        {
            Filters.Name = searchString;
            Filters.Published = publishedFilter;
            this.isPreview = isPreview;
            var user= await LoadDataAsync(Filters);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }
            return Page();
        }

        
        private async Task<SportsHubUser> LoadDataAsync(Filter Filters)
        {
            var user = await _userManager.GetUserAsync(User);
            Videos = await _context.Videos.ToListAsync();
            Videos = Videos.Reverse();
            Categories= new CategoriesIndexViewModel
            {
                Categories = _categoryManager.GetAllCategories<CategoryViewModel>().ToList(),
            };

            if (!String.IsNullOrEmpty(Filters.Name))
            {
                Videos = Videos.Where(x => x.Title.ToLower().Contains(Filters.Name.ToLower()));
            }
            if (Filters.Published!= PublishedSelector.All)
            {
                if (Filters.Published == PublishedSelector.Published)
                    Videos = Videos.Where(x => x.isPublished);
                else
                    Videos = Videos.Where(x => !x.isPublished);
            }
            return user;
        }

        //Redirecting to CreateVideo after submitting the New Video modal, passing the Video's url and wether it is a link or a file path

        public async Task<IActionResult> OnPostAsync()
        {
            if (User == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (!string.IsNullOrWhiteSpace(Input.VideoURL))
            {
                var data=await VideoData.GetData(Input.VideoURL);
                if (data.error != null)
                {
                    await LoadDataAsync(Filters);
                    ModelState.AddModelError("Input.VideoURL", "Invalid video (Video may not be authorized)");
                    return Page();
                }
                Input.VideoURL=ExtractCode(data.url);
                Title = data.title;
                isLink = true;
            }
            else if (Input.VideoFile != null)
            {
                Input.VideoURL = UploadedFile(Input.VideoFile, "uploads/videos", _webHostEnvironment);
            }
            else
            {
                await LoadDataAsync(Filters);
                return Page();
            }
            return RedirectToPage("VideoCreate", new VideoModel{ URL=Input.VideoURL,Title=Title,IsLink=isLink });
        }

        public async Task<IActionResult> OnPostDeleteAsync(int id)
        {
            var video=await _context.Videos.FindAsync(id);
            _context.Videos.Remove(video);
            _context.SaveChanges();
            await LoadDataAsync(Filters);
            return Page();
        }
        public async Task<IActionResult> OnPostPublishAsync(int id)
        {
            var video = await _context.Videos.FindAsync(id);
            video.isPublished = !video.isPublished;
            video.PublishDate = DateTime.Now;
            _context.Videos.Update(video);
            _context.SaveChanges();
            await LoadDataAsync(Filters);
            return Page();
        }

        //Uploads the selected file with generated code in a certain foulder 
        public static string UploadedFile(IFormFile file, string foulderPath,IWebHostEnvironment _webhost)
        {
            string uniqueFileName = null;

            if (file != null)
            {
                string uploadsFolder = Path.Combine(_webhost.WebRootPath, foulderPath);
                uniqueFileName = Guid.NewGuid().ToString() + file.FileName[file.FileName.LastIndexOf('.')..];
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using var fileStream = new FileStream(filePath, FileMode.Create);
                file.CopyTo(fileStream);
            }
            return uniqueFileName;
        }

        //Extracts the Youtube id of a youtube link
        public static string ExtractCode(string url)
        {
            return url[(url.LastIndexOf("watch?v=") + 8)..];
        }

    }
}
