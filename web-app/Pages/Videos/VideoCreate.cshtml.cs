using BAL.DTOs.Categories;
using BAL.Interfaces.Managers;
using DAL.EF;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;

namespace web_app.Pages.Video
{
    [Authorize(Roles = "Admin")]
    public class VideoCreateModel : PageModel
    {
        private readonly UserManager<SportsHubUser> _userManager;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly ApplicationDbContext _context;
        private readonly ICategoryManager _categoryManager;


        public VideoCreateModel(ICategoryManager categoryService,
            UserManager<SportsHubUser> userManager,
            IWebHostEnvironment webHostEnvironment,
            ApplicationDbContext context)
        {
            _categoryManager = categoryService;
            _userManager = userManager;
            _webHostEnvironment = webHostEnvironment;
            _context = context;
        }
        public class InputModel
        {
            public string VideoURL { get; set; }

            public IFormFile VideoFile { get; set; }

        }

        [BindProperty]
        public InputModel Input { get; set; }
        [BindProperty]
        public VideoModel CurrentVideo { get; set; }
        public CategoriesIndexViewModel Categories { get; private set; }

        public IActionResult OnGet(VideoModel video)
        {
            CurrentVideo = video;
            Categories = new CategoriesIndexViewModel
            {
                Categories = _categoryManager.GetAllCategories<CategoryViewModel>().ToList(),
            };
            return Page();
        }

        public virtual async Task<IActionResult> OnPostAsync()
        {
            if (User == null)
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");

            Categories = new CategoriesIndexViewModel
            {
                Categories = _categoryManager.GetAllCategories<CategoryViewModel>().ToList(),
            };

            if (!string.IsNullOrWhiteSpace(Input.VideoURL))
            {
                var data = await VideoData.GetData(Input.VideoURL);
                if (data.error != null)
                {
                    Input.VideoURL = "";
                    ModelState.AddModelError("Input.VideoURL", "Invalid video (Video may not be authorized)");
                    return Page();
                }
                CurrentVideo.URL = VideoPageModel.ExtractCode(data.url);
                CurrentVideo.Title = data.title;
                CurrentVideo.IsLink = true;
            }
            else if (Input.VideoFile != null)
            {
                CurrentVideo.URL = VideoPageModel.UploadedFile(Input.VideoFile, "uploads/videos", _webHostEnvironment);
                CurrentVideo.IsLink = false;
                CurrentVideo.Title = "Set Title";
            }
            return Page();
        }
        public virtual async Task<IActionResult> OnPostSaveAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (User == null)
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");

            if (!ModelState.IsValid)
                return Redirect(Request.Path);


            CurrentVideo.Uploader = user;
            CurrentVideo.isPublished = false;

            await _context.Videos.AddAsync(CurrentVideo);
            await _context.SaveChangesAsync();
            return RedirectToPage("Video");
        }

    }
}
