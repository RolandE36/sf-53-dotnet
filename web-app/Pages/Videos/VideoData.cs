﻿using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace web_app.Pages.Video
{
    public class VideoData
    {
        public string url { get; set; }
        public string title { get; set; }
        public string error { get; set; }
        public static async Task<VideoData> GetData(string url)
        {
            HttpClient httpClient = new HttpClient();
            var streamTask = httpClient.GetStreamAsync("https://noembed.com/embed?url=" + url);
            var response = await JsonSerializer.DeserializeAsync<VideoData>(await streamTask);
            return response;
        }
    }

}
