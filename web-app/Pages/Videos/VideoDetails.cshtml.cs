﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL.EF;
using DAL.Models;
using Microsoft.AspNetCore.Identity;

namespace web_app.Pages.Videos
{
    public class VideoDetailsModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<SportsHubUser> _userManager;


        public VideoDetailsModel(ApplicationDbContext context,UserManager<SportsHubUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public List<VideoModel> Videos { get; private set; }
        public VideoModel VideoModel { get; set; }
        [BindProperty]
        public string Comment { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Videos = await _context.Videos.ToListAsync();
            VideoModel = await _context.Videos.Include(x=>x.Coments).ThenInclude(x=>x.Uploader).Include(x=>x.Uploader).FirstOrDefaultAsync(m => m.Id == id);
            if (VideoModel == null)
            {
                return NotFound();
            }
            VideoModel.Views++;
            _context.Videos.Update(VideoModel);
            await _context.SaveChangesAsync();
            return Page();
        }
        public async Task<IActionResult> OnPostAsync(int? id)
        {
            var user = await _userManager.GetUserAsync(User);
            if (User == null)
                return NotFound($"Unable to load user with ID '{user.Id}'.");

            Videos = await _context.Videos.ToListAsync();
            VideoModel = await _context.Videos.Include(x => x.Coments).ThenInclude(x => x.Uploader).Include(x => x.Uploader).FirstOrDefaultAsync(m => m.Id == id);
            VideoModel.Coments.Add(new ComentsModel() { Uploader = user, Details = Comment, PublishDate = DateTime.Now });
            _context.Videos.Update(VideoModel);
            await _context.SaveChangesAsync();
            return Page();
        }
        public async Task<IActionResult> OnPostDeleteAsync(int? id,int? commentID)
        {
            var user = await _userManager.GetUserAsync(User);
            if (User == null)
                return NotFound($"Unable to load user with ID '{user.Id}'.");

            Videos = await _context.Videos.ToListAsync();
            VideoModel = await _context.Videos.Include(x => x.Coments).ThenInclude(x => x.Uploader).Include(x => x.Uploader).FirstOrDefaultAsync(m => m.Id == id);
            var DeletedComment = VideoModel.Coments.FirstOrDefault(m => m.Id == commentID);

            _context.Comments.Remove(DeletedComment);
            VideoModel.Coments.Remove(DeletedComment);
            _context.Videos.Update(VideoModel);
            await _context.SaveChangesAsync();
            return Page();
        }
    }
}
