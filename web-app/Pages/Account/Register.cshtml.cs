using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using DAL.EF;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace web_app.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<SportsHubUser> _signInManager;
        private readonly UserManager<SportsHubUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;

        public RegisterModel(
            UserManager<SportsHubUser> userManager,
            SignInManager<SportsHubUser> signInManager,
            ILogger<RegisterModel> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {
            [Display(Name = "Name")]
            public string FirstName { get; set; }

            [Display(Name = "Last Name")]
            public string LastName { get; set; }

            [Required]
            [EmailAddress(ErrorMessage = "Please enter valid email")]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [MinLength(8, ErrorMessage = "The {0} must be at least {1} characters long")]
            [RegularExpression("(?=.*[0-9])(?=.*[a-zA-Z])(.+)", ErrorMessage = "Password must contain at least 8 characters (letters and numbers)")]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }
        }

        private void SetViewData()
        {
            ViewData["Title"] = "Register";
            ViewData["OtherOptionSpan"] = "Already have an account?";
            ViewData["OtherOptionLink"] = "/Account/Login";
            ViewData["OtherOption"] = "Log In";
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            SetViewData();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl ??= Url.Content("~/Account/Login");
            if (ModelState.IsValid)
            {
                //Adjusted Register with default values for SportsHubUser
                var user = new SportsHubUser { UserName = Input.Email, Email = Input.Email, FirstName=Input.FirstName, LastName=Input.LastName, ProfilePicturePath="defaultProfilePicture.jpg" };
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");
                    
                    await _userManager.AddToRoleAsync(user, "User");
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return LocalRedirect(returnUrl);
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            SetViewData();
            return Page();
        }
    }
}