using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using DAL.EF;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace web_app.Pages.Account
{
    [Authorize(Roles = "Admin, User")]
    public class PersonalInfoModel : PageModel
    {
        private readonly SignInManager<SportsHubUser> _signInManager;
        private readonly UserManager<SportsHubUser> _userManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public PersonalInfoModel(
            UserManager<SportsHubUser> userManager,
            SignInManager<SportsHubUser> signInManager,
            IWebHostEnvironment webHostEnvironment)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }


        [BindProperty]
        public PersonalModel InputPersonal { get; set; }
        [BindProperty]
        public PasswordModel InputPassword { get; set; }

        public string onLoadTab { get; set; }
        public class PersonalModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "EMAIL")]
            public string Email { get; set; }

            [Required]
            [Display(Name = "FIRST NAME")]
            public string FirstName { get; set; }

            [Required]
            [Display(Name = "LAST NAME")]
            public string LastName { get; set; }

            public string ProfilePicturePath { get; set; }

            public IFormFile PictureFile { get; set; }

        }
        public class PasswordModel
        {
            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "CURRENT PASSWORD")]
            public string OldPassword { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$",
         ErrorMessage = "Password must include 1 uppercase character,1 lowercase character,1 special symbol and 1 number")]
            [Display(Name = "NEW PASSWORD")]
            public string NewPassword { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "CONFIRM PASSWORD")]
            [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$",
         ErrorMessage = "Password must include 1 uppercase character,1 lowercase character,1 special symbol and 1 number")]
            [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }
        private async Task LoadPersonalAsync(SportsHubUser user)
        {

            InputPersonal = new PersonalModel
            {
                Email = await _userManager.GetEmailAsync(user),
                FirstName = user.FirstName,
                LastName = user.LastName,
                ProfilePicturePath = user.ProfilePicturePath,
                PictureFile = null
            };
        }

        public async Task<IActionResult> OnGetAsync(string tab="personalInfo")
        {
            var user = await _userManager.GetUserAsync(User);
            onLoadTab = tab;
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await LoadPersonalAsync(user);
            this.InputPassword = new PasswordModel();
            return Page();
        }

        public async Task<IActionResult> OnPostPersonalAsync()
        {

            var user = await _userManager.GetUserAsync(User);
            foreach (PropertyInfo prop in InputPassword.GetType().GetProperties())
            {
                ModelState.Remove(prop.Name);
            }
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadPersonalAsync(user);
                onLoadTab = "personalInfo";
                return Page();
            }

            if (InputPersonal.FirstName != user.FirstName)
            {
                user.FirstName = InputPersonal.FirstName;
            }
            if (InputPersonal.LastName != user.LastName)
            {
                user.LastName = InputPersonal.LastName;
            }
            var email = await _userManager.GetEmailAsync(user);
            if (InputPersonal.Email != email)
            {
                user.Email=InputPersonal.Email;
            }
            var newPath = UploadedFile(InputPersonal);
            if (newPath != user.ProfilePicturePath && InputPersonal.PictureFile!=null)
            {
                user.ProfilePicturePath = newPath;
            }
            await _userManager.UpdateAsync(user);
            await _signInManager.RefreshSignInAsync(user);
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostPasswordAsync()
        {

            var user = await _userManager.GetUserAsync(User);
            foreach (PropertyInfo prop in InputPersonal.GetType().GetProperties())
            {
                ModelState.Remove(prop.Name);
            }
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadPersonalAsync(user);
                onLoadTab = "changePassword";
                return Page();
            }
            if (InputPassword.NewPassword == InputPassword.ConfirmPassword)
            {
                var changePasswordResult = await _userManager.ChangePasswordAsync(user, InputPassword.OldPassword, InputPassword.NewPassword);
                if (!changePasswordResult.Succeeded)
                {
                    foreach (var error in changePasswordResult.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                    return Page();
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "New Password and Confirm Password don't match.");
            }

            await _signInManager.RefreshSignInAsync(user);
            return RedirectToPage();
        }


        private string UploadedFile(PersonalModel InputPersonal)
        {
            string uniqueFileName = null;

            if (InputPersonal.PictureFile != null)
            {
                string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "uploads/profilePictures");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + InputPersonal.PictureFile.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    InputPersonal.PictureFile.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }
    }
}