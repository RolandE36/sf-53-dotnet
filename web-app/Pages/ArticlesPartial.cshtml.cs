﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DAL.EF;
using DAL.Models;

namespace web_app.Pages
{
    public class ArticlesPartialModel : PageModel
    {
        private readonly DAL.EF.ApplicationDbContext _context;


        public ArticlesPartialModel(DAL.EF.ApplicationDbContext context)
        {
            _context = context;

        }

        // Create local objects for Categories, Teams and Locations
        public IList<Article> Article { get; set; }

        public IList<Subcategory> Category { get; set; }
        public IList<Team> Team { get; set; }
        public IList<Location> Location { get; set; }

        public IList<ImageUrl> ImageUrl { get; set; }

        //wait to take aricle, category, location, team, url from the db
        public async Task OnGetAsync()
        {
            Article = await _context.Articles.ToListAsync();

            Category = await _context.Subcategories.ToListAsync();
            Location = await _context.Locations.ToListAsync();
            Team = await _context.Teams.ToListAsync();
            ImageUrl = await _context.Images.ToListAsync();
        }

    }
}
