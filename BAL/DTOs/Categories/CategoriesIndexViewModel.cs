﻿namespace BAL.DTOs.Categories
{
    using System.Collections.Generic;

    public class CategoriesIndexViewModel
    {
        public List<CategoryViewModel> Categories { get; set; }
    }
}
