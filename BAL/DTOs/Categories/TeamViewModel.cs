﻿using BAL.Mapping;
using DAL.Models;

namespace BAL.DTOs.Categories
{
    public class TeamViewModel : IMapFrom<Team>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string TrimmedName => Name.Trim();

        public bool IsHidden { get; set; }
    }
}
