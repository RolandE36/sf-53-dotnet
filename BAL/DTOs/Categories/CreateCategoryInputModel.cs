﻿namespace BAL.DTOs.Categories
{
    using System.ComponentModel.DataAnnotations;

    using static web_app.Common.GlobalConstants;

    public class CreateCategoryInputModel
    {
        [Required]
        [MinLength(CATEGORY_NAME_MIN_LENGTH, ErrorMessage = CATEGORY_NAME_INPUT_ERROR_MESSAGE)]
        [MaxLength(CATEGORY_NAME_MAX_LENGTH)]
        public string Name { get; set; }
    }
}
