﻿using BAL.Mapping;
using DAL.Models;
using System.Collections.Generic;

namespace BAL.DTOs.Categories
{
    public class CategoryViewModel : IMapFrom<Category>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string TrimmedName => Name.Trim();

        public bool IsHidden { get; set; }

        public IEnumerable<SubcategoryViewModel> Subcategories { get; set; }
    }
}
