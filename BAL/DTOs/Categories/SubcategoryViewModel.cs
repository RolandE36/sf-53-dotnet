﻿using BAL.Mapping;
using DAL.Models;
using System.Collections.Generic;

namespace BAL.DTOs.Categories
{
    public class SubcategoryViewModel : IMapFrom<Subcategory>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsHidden { get; set; }

        public IEnumerable<TeamViewModel> Teams { get; set; }
    }
}
