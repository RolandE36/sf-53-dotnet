﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BAL.Interfaces.Managers
{
    public interface ICategoryManager
    {
        IEnumerable<T> GetAllCategories<T>();

        IEnumerable<T> GetAllSubcategories<T>(int categoryId);

        IEnumerable<T> GetAllTeams<T>(int subcategoryId);

        Task AddCategories(List<string> categories);

        Task AddSubcategories(Dictionary<string, List<string>> subcategories);

        Task AddTeams(Dictionary<string, List<string>> teams);

        Task HideTeams(List<KeyValuePair<string, bool>> teams);

        Task HideSubcategories(List<KeyValuePair<string, bool>> subcategories);

        Task HideCategories(List<KeyValuePair<string, bool>> categories);

        Task EditCategories(List<KeyValuePair<string, string>> categories);

        Task EditSubcategories(List<KeyValuePair<string, string>> subcategories);

        Task EditTeams(List<KeyValuePair<string, string>> teams);

        Task MoveTeams(List<KeyValuePair<string, string>> teams, Dictionary<string, List<string>> newTeams);

        Task MoveSubcategories(List<KeyValuePair<string, string>> subcategories, Dictionary<string, List<string>> newSubcategories);

        Task DeleteTeams(List<string> teams);

        Task DeleteSubcategories(List<string> subcategories);

        Task DeleteCategories(List<string> categories);
    }
}
