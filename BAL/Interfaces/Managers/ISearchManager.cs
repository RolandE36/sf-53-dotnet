﻿using System.Threading.Tasks;
using System.Collections.Generic;

using DAL.Models;

namespace BAL.Interfaces.Managers
{
    public interface ISearchManager
    {
        public string SearchString { set; }
        public Task FilterData();
        public IList<SearchItemModel> GetSearchResults();
    }
}