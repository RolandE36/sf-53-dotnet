﻿using DAL.Models;
using System.Collections.Generic;
using DAL.Repositories;
using System.Threading.Tasks;
using BAL.Mapping;
using System.Linq;
using BAL.Interfaces.Managers;

namespace BAL.Managers
{
    public class CategoryManager : ICategoryManager
    {
        private readonly IRepository<Category> _categoriesRepository;
        private readonly IRepository<Subcategory> _subcategoriesRepository;
        private readonly IRepository<Team> _teamsRepository;

        public CategoryManager(IRepository<Category> categoriesRepository, IRepository<Subcategory> subcategoriesRepository, IRepository<Team> teamsRepository)
        {
            _categoriesRepository = categoriesRepository;
            _subcategoriesRepository = subcategoriesRepository;
            _teamsRepository = teamsRepository;
        }

        public IEnumerable<T> GetAllCategories<T>()
        {
            var categories = _categoriesRepository.AllAsNoTracking().To<T>().ToList();
            return categories;
        }

        public IEnumerable<T> GetAllSubcategories<T>(int categoryId)
        {
            var subcategories = _subcategoriesRepository.AllAsNoTracking().Where(s => s.Category.Id == categoryId).To<T>().ToList();
            return subcategories;
        }

        public IEnumerable<T> GetAllTeams<T>(int subcategoryId)
        {
            var teams = _teamsRepository.AllAsNoTracking().Where(t => t.Subcategory.Id == subcategoryId).To<T>().ToList();
            return teams;
        }

        public async Task AddCategories(List<string> categories)
        {
            foreach (var categoryName in categories)
            {
                var newCategory = new Category
                {
                    Name = categoryName,
                };

                await _categoriesRepository.AddAsync(newCategory);
                await _categoriesRepository.SaveChangesAsync();
            }
        }

        public async Task AddSubcategories(Dictionary<string, List<string>> subcategories)
        {
            foreach (var pair in subcategories)
            {
                var category = _categoriesRepository.AllAsNoTracking().Where(c => c.Name == pair.Key).FirstOrDefault();

                foreach (var subcategoryName in pair.Value)
                {
                    var subcategory = new Subcategory
                    {
                        Name = subcategoryName,
                        CategoryId = category.Id,
                    };

                    await _subcategoriesRepository.AddAsync(subcategory);
                    await _subcategoriesRepository.SaveChangesAsync();
                }
            }
        }

        public async Task AddTeams(Dictionary<string, List<string>> teams)
        {
            foreach (var pair in teams)
            {
                var subcategory = _subcategoriesRepository.AllAsNoTracking().Where(s => s.Name == pair.Key).FirstOrDefault();

                foreach (var teamName in pair.Value)
                {
                    var team = new Team
                    {
                        Name = teamName,
                        SubcategoryId = subcategory.Id,
                    };

                    await _teamsRepository.AddAsync(team);
                    await _teamsRepository.SaveChangesAsync();
                }
            }
        }

        public async Task HideTeams(List<KeyValuePair<string, bool>> teams)
        {
            foreach (var teamPair in teams)
            {
                var team = _teamsRepository.All().Where(t => t.Name == teamPair.Key).FirstOrDefault();

                if (team != null)
                {
                    team.IsHidden = teamPair.Value;
                    _teamsRepository.Update(team);
                    await _teamsRepository.SaveChangesAsync();
                }
            }
        }

        public async Task HideSubcategories(List<KeyValuePair<string, bool>> subcategories)
        {
            foreach (var subcategoryPair in subcategories)
            {
                var subcategory = _subcategoriesRepository.All().Where(s => s.Name == subcategoryPair.Key).FirstOrDefault();

                if (subcategory != null)
                {
                    subcategory.IsHidden = subcategoryPair.Value;
                    _subcategoriesRepository.Update(subcategory);
                    await _subcategoriesRepository.SaveChangesAsync();
                }
            }
        }

        public async Task HideCategories(List<KeyValuePair<string, bool>> categories)
        {
            foreach (var categoryPair in categories)
            {
                var category = _categoriesRepository.All().Where(c => c.Name == categoryPair.Key).FirstOrDefault();

                if (category != null)
                {
                    category.IsHidden = categoryPair.Value;
                    _categoriesRepository.Update(category);
                    await _categoriesRepository.SaveChangesAsync();
                }
            }
        }

        public async Task EditCategories(List<KeyValuePair<string, string>> categories)
        {
            foreach (var categoryPair in categories)
            {
                var category = _categoriesRepository.AllAsNoTracking().Where(c => c.Name == categoryPair.Key).FirstOrDefault();

                if (category != null)
                {
                    category.Name = categoryPair.Value;
                    _categoriesRepository.Update(category);
                    await _categoriesRepository.SaveChangesAsync();
                }
            }
        }

        public async Task EditSubcategories(List<KeyValuePair<string, string>> subcategories)
        {
            foreach (var subcategoryPair in subcategories)
            {
                var subcategory = _subcategoriesRepository.AllAsNoTracking().Where(s => s.Name == subcategoryPair.Key).FirstOrDefault();

                if (subcategory != null)
                {
                    subcategory.Name = subcategoryPair.Value;
                    _subcategoriesRepository.Update(subcategory);
                    await _subcategoriesRepository.SaveChangesAsync();
                }
            }
        }

        public async Task EditTeams(List<KeyValuePair<string, string>> teams)
        {
            foreach (var teamPair in teams)
            {
                var team = _teamsRepository.AllAsNoTracking().Where(t => t.Name == teamPair.Key).FirstOrDefault();

                if (team != null)
                {
                    team.Name = teamPair.Value;
                    _teamsRepository.Update(team);
                    await _teamsRepository.SaveChangesAsync();
                }
            }
        }

        public async Task MoveTeams(List<KeyValuePair<string, string>> teams, Dictionary<string, List<string>> newTeams)
        {
            foreach (var teamPair in teams)
            {
                var team = _teamsRepository.AllAsNoTracking().Where(t => t.Name == teamPair.Value).FirstOrDefault();
                var subcategory = _subcategoriesRepository.AllAsNoTracking().Where(s => s.Name == teamPair.Key).FirstOrDefault();

                if (team != null && subcategory != null)
                {
                    team.SubcategoryId = subcategory.Id;

                    if (newTeams.ContainsKey(subcategory.Name))
                    {
                        if (!newTeams[subcategory.Name].Contains(team.Name))
                        {
                            _teamsRepository.Update(team);
                            await _teamsRepository.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        _teamsRepository.Update(team);
                        await _teamsRepository.SaveChangesAsync();
                    }
                }
            }
        }

        public async Task MoveSubcategories(List<KeyValuePair<string, string>> subcategories, Dictionary<string, List<string>> newSubcategories)
        {
            foreach (var subcategoryPair in subcategories)
            {
                var subcategory = _subcategoriesRepository.AllAsNoTracking().Where(s => s.Name == subcategoryPair.Value).FirstOrDefault();
                var category = _categoriesRepository.AllAsNoTracking().Where(c => c.Name == subcategoryPair.Key).FirstOrDefault();

                if (subcategory != null && category != null)
                {
                    subcategory.CategoryId = category.Id;

                    if (newSubcategories.ContainsKey(category.Name))
                    {
                        if (!newSubcategories[category.Name].Contains(subcategory.Name))
                        {
                            _subcategoriesRepository.Update(subcategory);
                            await _subcategoriesRepository.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        _subcategoriesRepository.Update(subcategory);
                        await _subcategoriesRepository.SaveChangesAsync();
                    }
                }
            }
        }

        public async Task DeleteTeams(List<string> teams)
        {
            foreach (var teamName in teams)
            {
                var team = _teamsRepository.AllAsNoTracking().Where(t => t.Name == teamName).FirstOrDefault();

                if (team != null)
                {
                    _teamsRepository.Delete(team);
                    await _teamsRepository.SaveChangesAsync();
                }
            }
        }

        public async Task DeleteSubcategories(List<string> subcategories)
        {
            foreach (var subcategoryName in subcategories)
            {
                var subcategory = _subcategoriesRepository.AllAsNoTracking().Where(s => s.Name == subcategoryName).FirstOrDefault();

                if (subcategory != null)
                {
                    _subcategoriesRepository.Delete(subcategory);
                    await _subcategoriesRepository.SaveChangesAsync();
                }
            }
        }

        public async Task DeleteCategories(List<string> categories)
        {
            foreach (var categoryName in categories)
            {
                var category = _categoriesRepository.AllAsNoTracking().Where(c => c.Name == categoryName).FirstOrDefault();

                if (category != null)
                {
                    _categoriesRepository.Delete(category);
                    await _categoriesRepository.SaveChangesAsync();
                }
            }
        }
    }
}
