﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using DAL.EF;
using DAL.Models;
using BAL.Extensions;
using BAL.Interfaces.Managers;

namespace BAL.Managers
{
    public class SearchManager: ISearchManager
    {
        private readonly ApplicationDbContext _context;
        private IList<Article> _articles;
        private IList<VideoModel> _videos;

        public SearchManager(ApplicationDbContext context)
        {
            _context = context;
        }

        public string SearchString { get; set; } = string.Empty;
        
        public async Task FilterData()
        {
            string searchStringLowerCase = SearchString.ToLower();

            // Filter videos
            _videos = _context.Videos.AsEnumerable().Where(v =>
                v.Title.Contains(searchStringLowerCase, StringComparison.OrdinalIgnoreCase)
            ).ToList();

            // Set categories for articles
            _articles = _context.Articles.ToList();
            foreach (var article in _articles)
            {
                article.SubCategory = await _context.Subcategories.FindAsync(article.SubcategoryId);
                if (article.SubCategory != null)
                {
                    article.SubCategory.Category = await _context.Categories.FindAsync(article.SubCategory.CategoryId);
                }
            }
            
            // Filter articles
            _articles = _articles.Where(a =>
                a.Caption.Contains(searchStringLowerCase, StringComparison.OrdinalIgnoreCase) ||
                a.Headline.Contains(searchStringLowerCase, StringComparison.OrdinalIgnoreCase) ||
                SearchArticleCategories(a)
            ).ToList();
        }

        private bool SearchArticleCategories(Article article)
        {
            string searchStringLowerCase = SearchString.ToLower();
            
            if (article.SubCategory == null)
            {
                return false;
            }
            
            // Search in SubCategory
            if (article.SubCategory.Name.Contains(searchStringLowerCase, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
                
            // Search in Category
            return article.SubCategory.Category != null && 
                   article.SubCategory.Category.Name.Contains(searchStringLowerCase, StringComparison.OrdinalIgnoreCase);
        }

        public IList<SearchItemModel> GetSearchResults()
        {
            List<SearchItemModel> searchResults = new List<SearchItemModel>();
            
            // Add articles
            foreach (var article in _articles)
            {
                List<string> categorySequence = new List<string>();
                if (article.SubCategory != null)
                {
                    if (article.SubCategory.Category != null)
                    {
                        categorySequence.Add(article.SubCategory.Category.Name);
                    }
                    categorySequence.Add(article.SubCategory.Name);
                }
                searchResults.Add(new SearchItemModel()
                {
                    Title = article.Caption,
                    Description = article.Headline.MakeSubstringBold(SearchString),
                    Url = $"/Articles/ArticleView?id={article.Id}",
                    IsPictureUrl = false,
                    CategorySequence = categorySequence
                });
                
            }

            // Add videos
            searchResults.AddRange(_videos.Select(video => new SearchItemModel()
            {
                Title = video.Title,
                Description = video.PublishDate.ToString("MM/dd/yyyy").MakeSubstringBold(SearchString),
                Url = $"/Videos/VideoDetails?id={video.Id}",
                IsPictureUrl = true,
                PictureUrl = string.IsNullOrEmpty(video.URL) ? "/images/DefaultVideoPicture.jpeg" : $"https://img.youtube.com/vi/{video.URL}/hqdefault.jpg",
                CategorySequence = new List<string>()
            }));

            return searchResults;
        }
    }
}