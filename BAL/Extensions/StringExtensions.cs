﻿using System;

namespace BAL.Extensions
{
    public static class StringExtensions
    {
        private const string BoldTagOpen = "<b>";
        private const string BoldTagClose = "</b>";
        
        public static string MakeSubstringBold(this string source, string substring)
        {
            int tagLength = BoldTagOpen.Length + BoldTagClose.Length;
            string result = source;
            
            int startIndex = 0;
            while(true)
            {
                // Find SearchString occurence
                int searchIndex = result.IndexOf(substring, startIndex, StringComparison.CurrentCultureIgnoreCase);
                if (searchIndex == -1)
                {
                    break;
                }
                
                // Wrap SearchString occurence in bold tag
                result = result.Insert(searchIndex, BoldTagOpen);
                result = result.Insert(searchIndex + BoldTagOpen.Length + substring.Length, BoldTagClose);
                
                // Move startIndex
                startIndex = searchIndex + tagLength + substring.Length;
            }

            return result;
        }
    }
}