﻿namespace web_app.Common
{
    public static class GlobalConstants
    {
        // System & Roles Configuration
        public const string SYSTEM_NAME = "SportsHub";
        public const string ADMINISTRATOR_ROLE_NAME = "Admin";
        public const string USER_ROLE_NAME = "User";

        // Category Section Validation
        public const int CATEGORY_NAME_MIN_LENGTH = 2;
        public const int CATEGORY_NAME_MAX_LENGTH = 25;
        public const string CATEGORY_NAME_INPUT_ERROR_MESSAGE = "The category name must be between 2 and 25 characters.";
    }
}
